import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ContentLayoutComponent} from "./shared/layouts/content-layout/content-layout.component";
import {CONTENT_LAYOUT_ROUTES} from "./shared/routes/content-layout.routes";

const routes: Routes = [
  {
    path: '', redirectTo: 'product', pathMatch: 'full'
  },
  {
    path: '',
    component: ContentLayoutComponent,
    children: CONTENT_LAYOUT_ROUTES
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
