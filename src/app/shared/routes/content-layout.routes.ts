import {Routes} from "@angular/router";

export const CONTENT_LAYOUT_ROUTES: Routes = [
  {
    path: 'product',
    loadChildren: () => import('../../modules/product/product.module').then(m => m.ProductModule)
  }
];
